var modalError = $('#modalError');
var modalSuccess = $('#modalSuccess');

function mSuccess(message){
	modalSuccess.click();
	$("#mSuccess").html(message);
}

function mError(message){
	modalError.click();
	$("#mError").html(message);
}

function resetModal(){
	$("#mSuccess").html('');
	$("#mError").html('');
}

function signinProccess() {
	var data = {
		email: $("#email").val(),
		password: $("#password").val()
	};

	postData("auth_controller/signinProccess", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				window.location = "admin";
			} else {
				failed.click();
			}
		}
	});
}

function emailFormat() {
	var email = $("#email").val();
	if (email != '') {
		if (ValidateEmail(email, "#send_token", "#invalid_email")) {
			getEmail();
		}
	}else{
		$("#invalid_email").hide();
	}
}

function getEmail() {

	var data = {
		key: $("#email").val(),
		where: 'email',
		table: 'users'
	}

	postData('main_controller/getData', data, function (err, response) {
		if (response) {
			var status = response.status;
			if (status == 'success') {
				$("#notexist").show();
				$("#send_token").attr('disabled', 'disabled');
			} else {
				$("#notexist").hide();
				$('#send_token').removeAttr('disabled');
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

function send_token() {
	$("#send_token").html('Loading ..');
	var data = {
		email: $("#email").val()
	};

	postData("auth_controller/sendToken", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				$("#send_token").html('Send Token');
				mSuccess('Reset password link has been sent.');
			} else {
				$("#send_token").html('Send Token');
				mError('Failed to sent password reset link.');
			}
		}
	});
}