<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Type our email</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="emailFormat()" id="email" type="email" class="form-control" placeholder="Email" />
					<p id="invalid_email" class="btn btn-link btn-block" style="display:none;color:white;">Please enter the e-mail format correctly</p>
					<p id="notexist" class="btn btn-link btn-block" style="display:none;color:white;">Unregistered email</p>
				</div>

			</div>
			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Back to sign</a>
				</div>
				<div class="col-md-6">
					<a disabled id="send_token" onclick="send_token()" class="btn btn-info btn-block">Send Token</a>
				</div>
			</div>
		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 Backpropagation
		</div>
		<div class="pull-right">
		</div>
	</div>
</div>

<?php $this->load->view('auth/layout/footer')?>