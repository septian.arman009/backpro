<?php $this->load->view('auth/layout/header')?>
<div class="login-box animated fadeInDown">
	<!-- <div class="login-logo"></div> -->
	<div class="login-body">
		<div class="login-title">
			<strong>Create New Password</strong>
		</div>
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="checkPassword()" id="password" type="password" class="form-control"
						placeholder="Password" />
					<p id="passless" class="btn btn-link btn-block" style="display:none;color:white;">Password is at least 8 characters</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-12">
					<input onkeyup="checkPassword()" id="confirm" type="password" class="form-control"
						placeholder="Confirm Password" />
					<p id="conless" class="btn btn-link btn-block" style="display:none;color:white;">Password is at least 8 characters</p>
					<p id="notmatch" class="btn btn-link btn-block" style="display:none;color:white;">Password not match</p>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6">
					<a href="<?php echo base_url() ?>signin" class="btn btn-link btn-block">Back to sign</a>
				</div>
				<div class="col-md-6">
					<a disabled id="reset" onclick="resetPassword()" class="btn btn-info btn-block">Reset Password</a>
					<a style="display:none" id="loading" class="btn btn-info btn-block">Loading ..</a>
				</div>
			</div>

		</form>
	</div>
	<div class="login-footer">
		<div class="pull-left">
			&copy; 2019 Backpropagation
		</div>
		<div class="pull-right">

		</div>
	</div>
</div>

<?php $this->load->view('auth/layout/footer')?>

<script>
	function checkPassword() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}

		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}

		if (password != '' && confirm != '') {
			if (password == confirm) {
				$("#notmatch").hide();

				if (password.length >= 8 && confirm.length >= 8) {
					$("#reset").removeAttr('disabled');
				} else {
					$("#reset").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#reset").attr('disabled', 'disabled');
			}
		} else {
			$("#reset").attr('disabled', 'disabled');
		}
	}

	function resetPassword() {
		$("#reset").hide();
		$("#loading").show();
		var data = {
			email: '<?php echo $email ?>',
			password: $("#password").val()
		};

		postData("<?php echo base_url() ?>auth_controller/resetPassword", data, function (err, response) {
			if (response) {
				if (response.status == "success") {
					mSuccess('Password has been saved');
					setTimeout(() => {
						window.location = '<?php echo base_url() ?>signin';
					}, 1000);
				} else {
					mError('Failed to save password');
				}
			}
		});
	}
</script>