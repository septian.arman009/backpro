<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin/mycss/cropper.css" />
<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Upload Target</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Upload</strong> Target</h3>
					</div>

					<div class="panel-body">
						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Choose Image (Max 5)</label>
							<div class="col-md-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										<span class="fa fa-picture-o"></span>
									</span>
									<input type="file" name="file" id="fileInput" class="form-control">
									<span id="moveTo" onclick="updateTgtTrans()" style="cursor: pointer" class="input-group-addon">
										Update Target Transform</span>
								</div>
							</div>
						</div>
						<hr>
						<?php if($tImage > 0){ ?>
						<?php $data = 1; foreach ($target as $key => $value) { 
						if($data <= 4){ ?>
						<div class="col-md-3">
							<div class="panel panel-default">
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/target/<?php echo $value['name'] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $value['name'] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $value['rgb_value'] ?></p>
									<a onclick="destroy('<?php echo $value['id'] ?>')" class="btn btn-danger"
										id="delete">Hapus</a>
								</div>
							</div>
						</div>
						<?php }else{ ?>
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="panel panel-default">
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/target/<?php echo $value['name'] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $value['name'] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $value['rgb_value'] ?></p>
									<a onclick="destroy('<?php echo $value['id'] ?>')" class="btn btn-danger"
										id="delete">Hapus</a>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div>
						<?php } $data++; } } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="message-box mb-training animated fadeIn" id="mb-crop">
	<div class="mb-container mc-training">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-crop"></span>
				<strong>Crop Picture</strong></div>
			<div class="mb-content">
				<div class="img-container">
					<img id="image" style="display: none">
				</div>
				<div class="result-container">
					<img id="result" style="width: 500px">
				</div>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a style="display: none" class="btn btn-success btn-lg" id="crop">Crop</a>
					<a style="display: none" class="btn btn-success btn-lg" id="save">Save</a>
					<a style="display: none" class="btn btn-default btn-lg" id="restore">Restore</a>
					<a style="display: none" class="btn btn-default btn-lg" id="cancel">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url() ?>assets/admin/myjs/cropper.js"></script>

<script id="targetjs">
	var tImage = '<?php echo $tImage ?>';

	$("#target_spin").hide();
	$("#target_normal").show();

	var image = document.getElementById('image'),
		$result = document.getElementById('result');

	$('#fileInput').on('change', function () {
		if (tImage < 5) {
			var inputFile = $('input[name=file]');
			var fileToUpload = inputFile[0].files[0];
			var ext = fileToUpload['name'].search('jpg');
			var ext1 = fileToUpload['name'].search('jpeg');
			if (ext > 0 || ext1 > 0) {
				if (fileToUpload) {
					$('#mb-crop').show();
					reader = new FileReader();
					reader.onload = function () {
						image.src = reader.result;
					}
					reader.readAsDataURL(fileToUpload);

					setTimeout(() => {
						$('#crop').show();
						$('#cancel').show();
						var cropper = new Cropper(image, {
							viewMode: 1,
							ready: function () {
								var image = new Image();
								$('#crop').click(function () {
									image.src = cropper.getCroppedCanvas()
										.toDataURL('image/jpeg');
									$result.src = image.src;
									$(".img-container").hide();
									$('#result').show();
									$('#restore').show();
									$('#save').show();
									$('#crop').hide();
								});

								$('#save').click(function () {
									var formData = new FormData();
									formData.append("image", image.src);
									$("#save").html('Loading..');
									$.ajax({
										url: 'master_controller/saveTarget',
										type: 'post',
										data: formData,
										processData: false,
										contentType: false,
										success: function () {
											$("#save").html('Save');
											mSuccess(
												'File has been cropped'
											);
											loadView(
												'master_controller/targetUpload',
												'.content');
										}
									});
								});

								$('#restore').click(function () {
									$(".img-container").show();
									$('#result').hide();
									$('#restore').hide();
									$('#save').hide();
									$('#crop').show();
								});

								$('#cancel').click(function () {
									loadView('master_controller/targetUpload',
										'.content');
								});
							},
						});
						$("#image").show();
					}, 500);
				}
			} else {
				mError("Only jpg file allowed");
				$("#fileInput").val('');
			}
		} else {
			mError("Maximum image has been reached");
			$("#fileInput").val('');
		}
	});

	function destroy(id) {
		mConfirm('Are you sure want to delete ?', "doDestroy('" + id + "')");
	}

	function doDestroy(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/target/id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('master_controller/targetUpload', '.content');
					mSuccess('Target has been deleted');
				} else {
					mError('Failed to delete target');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function updateTgtTrans() {
		var data = {}
		postData('master_controller/updateTgtTrans/', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					mSuccess('Target transformation has been updated');
				} else {
					mError('Failed to update target transformation');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('targetjs').innerHTML = "";
</script>

<style>
	.img-container img {
		max-width: 500px;
	}

	.mc-training {
		height: 500px;
		overflow-y: scroll;
	}

	.mc-training::-webkit-scrollbar {
		width: 12px;
	}

	.mc-training::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
		border-radius: 10px;
	}

	.mc-training::-webkit-scrollbar-thumb {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
	}

	.result-container {
		margin: 0 auto;
		width: 500px;
	}

	.mb-training .mc-training {
		top: 0;
		height: 100vh;
	}
</style>