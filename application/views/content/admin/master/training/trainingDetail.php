<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin/mycss/cropper.css" />
<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li>Training Data</li>
	<li class="active">Training Detail</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Data</strong> List</h3>
					</div>
					<a onclick="sub_menu('#master_data','#trainings','master_controller/training')" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
					</a>

					<div class="panel-body">
						
						<div class="col-md-3">
							<div class="panel panel-default">
								<p style="padding: 10px;">Pic No. 1</p>
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/t_temps/<?php echo $image[0] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $image[0] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $training[0]['d1o'] ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="panel panel-default">
								<p style="padding: 10px;">Pic No. 2</p>
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/t_temps/<?php echo $image[1] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $image[1] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $training[0]['d2o'] ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="panel panel-default">
								<p style="padding: 10px;">Pic No. 3</p>
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/t_temps/<?php echo $image[2] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $image[2] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $training[0]['d3o'] ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="panel panel-default">
								<p style="padding: 10px;">Pic No. 4</p>
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/t_temps/<?php echo $image[3] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $image[3] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $training[0]['d4o'] ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="panel panel-default">
								<p style="padding: 10px;">Pic No. 5</p>
								<div style="margin:auto;text-align:center;height:45vh;margin-bottom:5%;margin-top:5%;"
									class="panel-body panel-body-image">
									<img style="width:200px; height:200px;"
										src="<?php echo base_url() ?>assets/t_temps/<?php echo $image[4] ?>">
								</div>
								<div class="panel-footer text-muted">
									<p style="color:black; font-size: 11px;"><?php echo $image[4] ?></p>
									<p style="color:black; font-size: 11px;">RGB to Decimal :
										<?php echo $training[0]['d5o'] ?></p>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>