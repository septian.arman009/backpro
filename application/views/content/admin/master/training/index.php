<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Training Data</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Data List</h3>
				</div>
				<div class="panel-body">
					<button onclick="trainTransform()"
						class="btn btn-default mb-control" data-box="#message-box-sound-2">Transformasi Data</button>
					<br>
					<br>
					<table id="training-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Name</th>
								<th id="th">Data 1</th>
								<th id="th">Data 2</th>
								<th id="th">Data 3</th>
								<th id="th">Data 4</th>
								<th id="th">Data 5</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Name</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>

		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Training Data Transformation</h3>
				</div>
				<div class="panel-body">
					<table id="transformation-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Name</th>
								<th id="th">Data 1 Transform</th>
								<th id="th">Data 2 Transform</th>
								<th id="th">Data 3 Transform</th>
								<th id="th">Data 4 Transform</th>
								<th id="th">Data 5 Transform</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no_transform</th>
								<th class="footer">Name</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>

	</div>
</div>

<script id="trainingjs">
	$(document).ready(function () {
		$("#trainings_spin").hide();
		$("#trainings_normal").show();

		$('#training-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' +
				title + '" />';
			$(this).html(inp);
		});

		var table = $('#training-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'master_controller/trainingTable',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});

		$('#transformation-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' +
				title + '" />';
			$(this).html(inp);
		});

		var tableTransform = $('#transformation-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'master_controller/transformationTable',
				"type": "POST"
			}
		});

		tableTransform.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});


		$("#no_transform").hide();
	});

	function destroy(id) {
		mConfirm('Are you sure want to delete ?', "doDestroy('" + id + "')");
	}

	function doDestroy(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/trainings/training_id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('master_controller/training', '.content');
					mSuccess('Training has been deleted');
				} else {
					mError('Failed to delete training');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function trainTransform() {
		var data = {}
		postData('master_controller/trainTransform', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('master_controller/training', '.content');
					mSuccess('Transform data complete');
				} else {
					mError('Failed to transform data');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function edit(id) {
		loadView('master_controller/trainingDetail/' + id, '.content');
	}

	document.getElementById('trainingjs').innerHTML = "";
</script>

<style>
	#training-table_filter {
		display: none;
	}
	#transformation-table_filter {
		display: none;
	}
</style>