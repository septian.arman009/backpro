<ul class="breadcrumb">
	<li>
		<a href="#">Data Master</a>
	</li>
	<li class="active">Users</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">User List</h3>
				</div>
				<div class="panel-body">
					<button onclick="loadView('master_controller/form/users/null/null', '.content')" class="btn btn-default mb-control"
					 data-box="#message-box-sound-2">Add User</button>
					<br>
					<br>
					<table id="user-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Name</th>
								<th id="th">Email</th>
								<th id="th">Roles</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Name</th>
								<th class="footer">Email</th>
								<th class="footer">Roles</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>

<script id="userjs">
	$(document).ready(function () {
		$("#users_spin").hide();
		$("#users_normal").show();

		$('#user-table tfoot th').each(function () {
			var title = $(this).text();
			var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
			$(this).html(inp);
		});

		var table = $('#user-table').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": 'master_controller/userTable',
				"type": "POST"
			}
		});

		table.columns().every(function () {
			var that = this;
			$('input', this.footer()).on('keyup change', function () {
				if (that.search() !== this.value) {
					that.search(this.value).draw();
				}
			});
		});

		$("#no").hide();
	});

	function destroy(id){
		mConfirm('Are you sure want to delete ?', "do_destroy('"+id+"')");
	}

	function do_destroy(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/users/user_id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('master_controller/user', '.content');
					mSuccess('User has been deleted');
				} else {
					mError('Failed to delete user');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function edit(id) {
		loadView('master_controller/form/users/' + id + '/null', '.content');
	}

	document.getElementById('userjs').innerHTML = "";
</script>

<style>
	#user-table_filter {
		display: none;
	}
</style>