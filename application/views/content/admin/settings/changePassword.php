<ul class="breadcrumb">
	<li>
		<a href="#">Settings</a>
	</li>
	<li class="active">Change Password</li>
</ul>

<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">

			<form class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Change</strong> Password</h3>
					</div>

					<div class="panel-body">

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Old Password</label>
							<div class="col-md-6 col-xs-12">
								<input id="old_password" onchange="checkPassword()" type="password" class="form-control">
								<span class="help-block" id="wrong" style="display:none;">Wrong password !</span>
                            </div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">New Password</label>
							<div class="col-md-6 col-xs-12">
								<input disabled id="password" onkeyup="check()" type="password" class="form-control">
								<span class="help-block" id="passless" style="display:none;">Password is at least 8 characters !</span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-3 col-xs-12 control-label">Confirm New Password</label>
							<div class="col-md-6 col-xs-12">
								<input disabled id="confirm" onkeyup="check()" type="password" class="form-control">
								<span class="help-block" id="conless" style="display:none;">Password is at least 8 characters !</span>
								<span class="help-block" id="notmatch" style="display:none;">Password is not match !</span>
							</div>
						</div>

					</div>

					<div class="panel-footer">
						<a disabled id="save" onclick="action()" class="btn btn-primary pull-right">Save</a>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>

<script id="changepasswordjs">
	$(document).ready(function () {
		$("#changePassword_spin").hide();
		$("#changePassword_normal").show();
		check();
	});

	function check() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();
		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen != '') {
			if (passlen < 8) {
				$("#passless").show();
			} else {
				$("#passless").hide();
			}
		} else {
			$("#passless").hide();
		}
		if (confirm != '') {
			if (conlen < 8) {
				$("#conless").show();
			} else {
				$("#conless").hide();
			}
		} else {
			$("#conless").hide();
		}
		if (password != '' && confirm != '') {
			if (password == confirm) {
				$("#notmatch").hide();
				if (password.length >= 8 && confirm.length >= 8) {
					$("#save").removeAttr('disabled');
				} else {
					$("#save").attr('disabled', 'disabled');
				}
			} else {
				$("#notmatch").show();
				$("#save").attr('disabled', 'disabled');
			}
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function checkPassword() {
		var data = {
			old_password: $('#old_password').val()
		}

		postData('main_controller/checkOldPassword', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#password").removeAttr('disabled');
					$("#confirm").removeAttr('disabled');
					$("#wrong").hide();
				} else {
					$("#password").attr('disabled', 'disabled');
					$("#confirm").attr('disabled', 'disabled');
					$("#wrong").show();
				}
			} else {
				console.log('error : ', err);
			}
		});
	}

	function action() {
		$("#save").html('Loading ..');
		var data = {
			password: $('#password').val()
		}

		postData('main_controller/updatePassword', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").html('Simpan');
					mSuccess('Ganti password berhasil');
					sub_menu('#settings', '#changePassword', 'main_controller/changePassword');
				} else {
					$("#save").html('Simpan');
					mError('Gagal mengganti password');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('changepasswordjs').innerHTML = "";
</script>