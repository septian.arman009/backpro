<body>
	<!-- START PAGE CONTAINER -->
	<div class="page-container page-navigation-top-fixed">

		<!-- START PAGE SIDEBAR -->
		<div class="page-sidebar page-sidebar-fixed scroll">
			<!-- START X-NAVIGATION -->
			<ul class="x-navigation">
				<li class="xn-logo">
					<a>AI of QC</a>
					<a class="x-navigation-control"></a>
				</li>

				<li class="xn-profile">
					<a class="profile-mini">
						<img src="<?php echo base_url() ?>assets/admin/img/jst.png" alt="John Doe" />
					</a>
					<div class="profile">
						<div class="profile-image">
							<img src="<?php echo base_url() ?>assets/admin/img/jst.png" alt="John Doe" />
						</div>
						<div class="profile-data">
							<div class="profile-data-name">
								<?php echo whoIAM()['name'] ?>
							</div>
							<div class="profile-data-title">
								<?php echo whoIAM()['role_name'] ?>
							</div>
						</div>
						<div class="profile-controls">
							<a onclick="sub_menu('#master_data','#users','master_controller/form/users/<?php echo whoIAM()['id'] ?>/null')" class="profile-control-left">
								<span class="fa fa-user"></span>
							</a>
							<a onclick="sub_menu('#settings','#changePassword','main_controller/changePassword')" class="profile-control-right">
								<span class="fa fa-lock"></span>
							</a>
						</div>
					</div>
				</li>

				<li class="xn-title">Main Navigation</li>

				<li id="home">
					<a onmousedown="spinner('#home_spin', '#home_normal')"
						onclick="main_menu('#home','main_controller/home/1')">
						<span class="fa fa-home" id="home_normal"></span>
						<span class="fa fa-refresh fa-spin" style="display: none;" id="home_spin"></span>
						<span class="xn-text">Halaman Utama</span>
					</a>
				</li>
				
				<li id="master_data" class="xn-openable">
					<a>
						<span class="fa fa-hdd-o"></span>
						<span class="xn-text">Data Master</span>
					</a>
					<ul>
						<li id="users">
							<a onmousedown="spinner('#users_spin','#users_normal')"
								onclick="sub_menu('#master_data','#users','master_controller/user')">
								<span class="fa fa-user" id="users_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="users_spin"></span>
								Users
							</a>
						</li>

						<li id="target">
							<a onmousedown="spinner('#target_spin','#target_normal')"
								onclick="sub_menu('#master_data','#target','master_controller/targetUpload')">
								<span class="fa fa-cloud-upload" id="target_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="target_spin"></span>
								Target Upload
							</a>
						</li>

						<li id="trainTemps">
							<a onmousedown="spinner('#trainTemps_spin','#trainTemps_normal')"
								onclick="sub_menu('#master_data','#trainTemps','master_controller/tTUpload')">
								<span class="fa fa-cloud-upload" id="trainTemps_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="trainTemps_spin"></span>
								Training Data Upload
							</a>
						</li>

						<li id="trainings">
							<a onmousedown="spinner('#trainings_spin','#trainings_normal')"
								onclick="sub_menu('#master_data','#trainings','master_controller/training')">
								<span class="fa fa-list" id="trainings_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="trainings_spin"></span>
								Training Data
							</a>
						</li>
						
					</ul>
				</li>

				<li id="backpro" class="xn-openable">
					<a>
						<span class="fa fa-code-fork"></span>
						<span class="xn-text">Backpropagation</span>
					</a>
					<ul>
						<li id="backTrain">
							<a onmousedown="spinner('#backTrain_spin','#backTrain_normal')"
								onclick="sub_menu('#backpro','#backTrain','backpro_controller/backTrain')">
								<span class="fa fa-gear" id="backTrain_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="backTrain_spin"></span>
								Backpro Learning
							</a>
						</li>	
						<li id="backTest">
							<a onmousedown="spinner('#backTest_spin','#backTest_normal')"
								onclick="sub_menu('#backpro','#backTest','simulation_controller/backTest/null')">
								<span class="fa fa-flask" id="backTest_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="backTest_spin"></span>
								Training Data Test
							</a>
						</li>	
						<li id="QTest">
							<a onmousedown="spinner('#QTest_spin','#QTest_normal')"
								onclick="sub_menu('#backpro','#QTest','simulation_controller/QTest/null')">
								<span class="fa fa-flash" id="QTest_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;" id="QTest_spin"></span>
								Quality Test
							</a>
						</li>						
					</ul>
				</li>

				<li id="settings" class="xn-openable">
					<a>
						<span class="fa fa-gear"></span>
						<span class="xn-text">Settings</span>
					</a>
					<ul>
						<li id="emailSystem">
							<a onmousedown="spinner('#emailSystem_spin','#emailSystem_normal')"
								onclick="sub_menu('#settings','#emailSystem','main_controller/emailSystem')">
								<span class="fa fa-envelope" id="emailSystem_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="emailSystem_spin"></span>
								Email System
							</a>
						</li>
						<li id="changePassword">
							<a onmousedown="spinner('#changePassword_spin','#changePassword_normal')"
								onclick="sub_menu('#settings','#changePassword','main_controller/changePassword')">
								<span class="fa fa-lock" id="changePassword_normal"></span>
								<span class="fa fa-refresh fa-spin" style="display: none;"
									id="changePassword_spin"></span>
								Change Password
							</a>
						</li>
					</ul>
				</li>

			</ul>
			<!-- END X-NAVIGATION -->
		</div>
		<!-- END PAGE SIDEBAR -->
	