<ul class="breadcrumb">
	<li>
		<a href="#">Backpropagation</a>
	</li>
	<li class="active">Backpro Learning</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Learning</strong> Proccess</h3>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<img width="400px" src="<?php base_url() ?>assets/551.jpg" alt="551 JST">
							</div>
							<div class="col-md-3"></div>
							<div class="col-md-3" style="margin-top: 10%">
								<img width="400px" src="<?php base_url() ?>assets/inisialisasi.png"
									alt="Inisialisasi JST">
							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="col-md-3 col-xs-12">
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<a id="textString">Hidden Weights</a> | 
								<a class="btn btn-primary" onclick="randomNumber()">Random Weight & Bias</a> |
								<a id="training" class="btn btn-primary" onclick="trainingStart()">Start Learning</a>
								<table class="table">
									<tr>
										<td id="header">X1</td>
										<td id="header">X2</td>
										<td id="header">X3</td>
										<td id="header">X4</td>
										<td id="header">X5</td>
									</tr>
									<tr>
										<td><input readonly id="v11" type="number" class="form-control" placeholder="v11">
										</td>
										<td><input readonly id="v21" type="number" class="form-control" placeholder="v21">
										</td>
										<td><input readonly id="v31" type="number" class="form-control" placeholder="v31">
										</td>
										<td><input readonly id="v41" type="number" class="form-control" placeholder="v41">
										</td>
										<td><input readonly id="v51" type="number" class="form-control" placeholder="v51">
										</td>
									</tr>
									<tr>
										<td><input readonly id="v12" type="number" class="form-control" placeholder="v12">
										</td>
										<td><input readonly id="v22" type="number" class="form-control" placeholder="v22">
										</td>
										<td><input readonly id="v32" type="number" class="form-control" placeholder="v32">
										</td>
										<td><input readonly id="v42" type="number" class="form-control" placeholder="v42">
										</td>
										<td><input readonly id="v52" type="number" class="form-control" placeholder="v52">
										</td>
									</tr>
									<tr>
										<td><input readonly id="v13" type="number" class="form-control" placeholder="v13">
										</td>
										<td><input readonly id="v23" type="number" class="form-control" placeholder="v23">
										</td>
										<td><input readonly id="v33" type="number" class="form-control" placeholder="v33">
										</td>
										<td><input readonly id="v43" type="number" class="form-control" placeholder="v43">
										</td>
										<td><input readonly id="v53" type="number" class="form-control" placeholder="v53">
										</td>
									</tr>
									<tr>
										<td><input readonly id="v14" type="number" class="form-control" placeholder="v14">
										</td>
										<td><input readonly id="v24" type="number" class="form-control" placeholder="v24">
										</td>
										<td><input readonly id="v34" type="number" class="form-control" placeholder="v34">
										</td>
										<td><input readonly id="v44" type="number" class="form-control" placeholder="v44">
										</td>
										<td><input readonly id="v54" type="number" class="form-control" placeholder="v54">
										</td>
									</tr>
									<tr>
										<td><input readonly id="v15" type="number" class="form-control" placeholder="v15">
										</td>
										<td><input readonly id="v25" type="number" class="form-control" placeholder="v25">
										</td>
										<td><input readonly id="v35" type="number" class="form-control" placeholder="v35">
										</td>
										<td><input readonly id="v45" type="number" class="form-control" placeholder="v45">
										</td>
										<td><input readonly id="v55" type="number" class="form-control" placeholder="v55">
										</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table class="table">
									<tr>
										<td id="header" width="20%">
											<p>Hidden BIAS (B1)</p>
										</td>
										<td id="header" width="20%">
											<p>Output Weights (Zx)
										</td>
										<td id="header" width="20%">
											<p>Output BIAS (B2)
										</td>
										<td id="header" width="20%">
											<p>Learning Rate
										</td>
										<td id="header" width="20%">
											<p>Learning Parameter
										</td>
									</tr>
									<tr>
										<td><input readonly id="v01" type="number" class="form-control" placeholder="v01">
										</td>
										<td><input readonly id="w1" type="number" class="form-control" placeholder="w1">
										</td>
										<td><input readonly id="w0" type="number" class="form-control" placeholder="w0">
										</td>
										<td><input id="lrInput" type="number" class="form-control"
												placeholder="LR Bobot Input" value="0.25"></td>
										<td><input id="mse" type="number" class="form-control" value="0.0001"
												placeholder="Means Square Error (MSE)"></td>
									</tr>
									<tr>
										<td><input readonly id="v02" type="number" class="form-control" placeholder="v02">
										</td>
										<td><input readonly id="w2" type="number" class="form-control" placeholder="w2">
										</td>
										<td></td>
										<td><input id="lrBiasInput" type="number"
												class="form-control" placeholder="LR Bias Input" value="0.25"></td>
										<td><input id="maxEpoch" type="number" class="form-control"
												placeholder="Mximum Epoch" value="10000"></td>
									</tr>
									<tr>
										<td><input readonly id="v03" type="number" class="form-control" placeholder="v03">
										</td>
										<td><input readonly id="w3" type="number" class="form-control" placeholder="w3">
										</td>
										<td></td>
										<td><input id="lrHidden" type="number" class="form-control"
												placeholder="LR Bobot Lapisan" value="0.25"></td>
										<td></td>
									</tr>
									<tr>
										<td><input readonly id="v04" type="number" class="form-control" placeholder="v04">
										</td>
										<td><input readonly id="w4" type="number" class="form-control" placeholder="w4">
										</td>
										<td></td>
										<td><input id="lrBiasHidden" type="number" class="form-control"
												placeholder="LR Bias Lapisan" value="0.25"></td>
										<td></td>
									</tr>
									<tr>
										<td><input readonly id="v05" type="number" class="form-control" placeholder="v05">
										</td>
										<td><input readonly id="w5" type="number" class="form-control" placeholder="w5">
										</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#backTrain_spin").hide();
	$("#backTrain_normal").show();

	function randomNumber(min, max) {
		var max = 0.5;
		var min = -0.5;
		$("#v11").val((Math.random() * (max - min) + min));
		$("#v21").val((Math.random() * (max - min) + min));
		$("#v31").val((Math.random() * (max - min) + min));
		$("#v41").val((Math.random() * (max - min) + min));
		$("#v51").val((Math.random() * (max - min) + min));

		$("#v12").val((Math.random() * (max - min) + min));
		$("#v22").val((Math.random() * (max - min) + min));
		$("#v32").val((Math.random() * (max - min) + min));
		$("#v42").val((Math.random() * (max - min) + min));
		$("#v52").val((Math.random() * (max - min) + min));

		$("#v13").val((Math.random() * (max - min) + min));
		$("#v23").val((Math.random() * (max - min) + min));
		$("#v33").val((Math.random() * (max - min) + min));
		$("#v43").val((Math.random() * (max - min) + min));
		$("#v53").val((Math.random() * (max - min) + min));

		$("#v14").val((Math.random() * (max - min) + min));
		$("#v24").val((Math.random() * (max - min) + min));
		$("#v34").val((Math.random() * (max - min) + min));
		$("#v44").val((Math.random() * (max - min) + min));
		$("#v54").val((Math.random() * (max - min) + min));

		$("#v15").val((Math.random() * (max - min) + min));
		$("#v25").val((Math.random() * (max - min) + min));
		$("#v35").val((Math.random() * (max - min) + min));
		$("#v45").val((Math.random() * (max - min) + min));
		$("#v55").val((Math.random() * (max - min) + min));

		$("#v01").val((Math.random() * (max - min) + min));
		$("#v02").val((Math.random() * (max - min) + min));
		$("#v03").val((Math.random() * (max - min) + min));
		$("#v04").val((Math.random() * (max - min) + min));
		$("#v05").val((Math.random() * (max - min) + min));

		$("#w0").val((Math.random() * (max - min) + min));
		$("#w1").val((Math.random() * (max - min) + min));
		$("#w2").val((Math.random() * (max - min) + min));
		$("#w3").val((Math.random() * (max - min) + min));
		$("#w4").val((Math.random() * (max - min) + min));
		$("#w5").val((Math.random() * (max - min) + min));

		this.weight = 'true';
	}

	function trainingStart(key_id) {
		if (key_id != 'null') {
			$("#training").html("Proccessing...");
			data = {
				v11: $("#v11").val(),
				v21: $("#v21").val(),
				v31: $("#v31").val(),
				v41: $("#v41").val(),
				v51: $("#v51").val(),

				v12: $("#v12").val(),
				v22: $("#v22").val(),
				v32: $("#v32").val(),
				v42: $("#v42").val(),
				v52: $("#v52").val(),

				v13: $("#v13").val(),
				v23: $("#v23").val(),
				v33: $("#v33").val(),
				v43: $("#v43").val(),
				v53: $("#v53").val(),

				v14: $("#v14").val(),
				v24: $("#v24").val(),
				v34: $("#v34").val(),
				v44: $("#v44").val(),
				v54: $("#v54").val(),

				v15: $("#v15").val(),
				v25: $("#v25").val(),
				v35: $("#v35").val(),
				v45: $("#v45").val(),
				v55: $("#v55").val(),

				v01: $("#v01").val(),
				v02: $("#v02").val(),
				v03: $("#v03").val(),
				v04: $("#v04").val(),
				v05: $("#v05").val(),

				w0: $("#w0").val(),
				w1: $("#w1").val(),
				w2: $("#w2").val(),
				w3: $("#w3").val(),
				w4: $("#w4").val(),
				w5: $("#w5").val(),

				lrInput: $("#lrInput").val(),
				lrBiasInput: $("#lrBiasInput").val(),
				lrHidden: $("#lrHidden").val(),
				lrBiasHidden: $("#lrBiasHidden").val(),
				mse: $("#mse").val(),
				maxEpoch: $("#maxEpoch").val()
			}

			postData('backpro_controller/trainingStart', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#training").html("Start Learning");
						mSuccess("Performance Goal Meet");
						loadView('backpro_controller/backTrain', '.content');
					}else if (response.status == 'mse_stop') {
						$("#training").html("Start Learning");
						mError("Performance Goal Not Meet, The network doesn't meet target mse");
						loadView('backpro_controller/backTrain', '.content');
					}else if (response.status == 'max_epoch') {
						$("#training").html("Start Learning");
						mError("Maximum Epoch Reach, The network doesn't meet target mse");
						loadView('backpro_controller/backTrain', '.content');
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}
</script>

<style>
	td {
		padding: 10px;
	}

	#header {
		text-align: center;
	}

	p {
		font-weight: bold;
	}

	#textString {
		color: black;
		font-weight: bold;
		text-decoration: none;
	}

	#v11, #v21, #v31, #v41, #v51,
	#v12, #v22, #v32, #v42, #v52,
	#v13, #v23, #v33, #v43, #v53,
	#v14, #v24, #v34, #v44, #v54,
	#v15, #v25, #v35, #v45, #v55,
	#v01, #v02, #v03, #v04, #v05,
	#w0, #w1, #w2, #w3, #w4, #w5 {
		color: black;
	}
</style>