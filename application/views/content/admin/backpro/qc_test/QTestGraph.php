<div id="mseGraph"></div>
<script>
	$(function () {
		var chart;
		$(document).ready(function () {
			$.getJSON("simulation_controller/mseGraph/<?php echo $learning_id ?>", function (json) {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'mseGraph',
						type: 'line'

					},
					title: {
						text: 'MSE <?php echo $mse ?>'

					},
					subtitle: {
						text: ''

					},
					credits: {
						enabled: false
					},
					xAxis: {
						categories: []
					},
					yAxis: {
						title: {
							text: 'Mean Square Error (MSE)'
						},
						plotLines: [{
							value: 0,
							width: 1,
							color: '#808080'
						}]
					},
					tooltip: {
						formatter: function () {
							return '<b>' + this.series.name + '</b><br/>' +
								this.x + ': ' + this.y;
						}
					},
					legend: {
						layout: 'vertical',
						align: 'right',
						verticalAlign: 'top',
						x: -10,
						y: 120,
						borderWidth: 0
					},
					series: json
				});
			});

		});

	});
</script>