<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin/mycss/cropper.css" />
<ul class="breadcrumb">
	<li>
		<a href="#">Backpropagation</a>
	</li>
	<li class="active">Quality Test</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Test</strong> Form</h3>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-6">
								<div class="mseGraph"></div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-3 col-xs-12 control-label">Choose Learning Sample</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-gear"></span></span>
											<select onchange="changeId($(this).val())" id="learning_id"
												class="form-control">
												<?php foreach ($learning as $key => $value) { ?>
												<?php if($value['learning_id'] == $learning_id){

                                                    if($value['last_mse'] < $value['target_mse']){?>
												<option selected value="<?php echo $value['learning_id'] ?>">Learning
													<?php echo "{$value['learning_id']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
												</option>
												<?php }else{?>
												<option selected value="<?php echo $value['learning_id'] ?>"
													style="color: red">Learning
													<?php echo "{$value['learning_id']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
												</option>
												<?php } ?>
												<?php }else {
                                                    if($value['last_mse'] < $value['target_mse']){ ?>
												<option value="<?php echo $value['learning_id'] ?>">Learning
													<?php echo "{$value['learning_id']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
												</option>
												<?php }else{?>
												<option value="<?php echo $value['learning_id'] ?>" style="color: red">
													Learning
													<?php echo "{$value['learning_id']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
												</option>
												<?php } ?>
												<?php } } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Test Identity</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-key"></span>
											</span>
											<input onkeyup="formCheck()" id="identity" type="text" class="form-control">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">Upload Sample</label>
									<div class="col-md-9 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">
												<span class="fa fa-image"></span>
											</span>
											<input type="file" name="file" id="fileInput" class="form-control">
										</div>
									</div>
								</div>
								<?php if($tImage > 0){ ?>
								<?php foreach ($testData as $key => $value) { ?>
								<div class="col-md-3">
									<div class="panel panel-default">
										<div style="margin:auto;text-align:center;height:50px;margin-bottom:5%;margin-top:5%;"
											class="panel-body panel-body-image">
											<img style="width:50px; height:50px;"
												src="<?php echo base_url() ?>assets/q_test/<?php echo $value['name'] ?>">
										</div>
										<div class="panel-footer text-muted">
											<a onclick="destroy('<?php echo $value['id'] ?>')" class="btn btn-danger" style="width: 100%">Hapus</a>
										</div>
									</div>
								</div>
								<?php } }?>
							</div>
						</div>
						<hr>
					</div>

					<div class="panel-footer">
						<a disabled id="startTest" onclick="startTest()" class="btn btn-primary pull-right">Start Test</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<div class="message-box mb-training animated fadeIn" id="mb-crop">
	<div class="mb-container mc-training">
		<div class="mb-middle">
			<div class="mb-title">
				<span class="fa fa-crop"></span>
				<strong>Crop Picture</strong></div>
			<div class="mb-content">
				<div class="img-container">
					<img id="image" style="display: none">
				</div>
				<div class="result-container">
					<img id="result" style="width: 500px">
				</div>
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<a style="display: none" class="btn btn-success btn-lg" id="crop">Crop</a>
					<a style="display: none" class="btn btn-success btn-lg" id="save">Save</a>
					<a style="display: none" class="btn btn-default btn-lg" id="restore">Restore</a>
					<a style="display: none" class="btn btn-default btn-lg" id="cancel">Cancel</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#QTest_spin").hide();
	$("#QTest_normal").show();
	changeId($("#learning_id").val());

	function changeId(learning_id) {
		loadToRender("simulation_controller/QTestGraph/" + learning_id, '.mseGraph');
	}

	var tImage = '<?php echo $tImage ?>';


	function formCheck() {
		var identity = $("#identity").val();

		if (identity != '' && tImage == 5) {
			$("#startTest").removeAttr("disabled");
		} else {
			$("#startTest").attr("disabled", "disabled");
		}
	}

	var image = document.getElementById('image'),
		$result = document.getElementById('result');

	$('#fileInput').on('change', function () {
		if (tImage < 5) {
			var inputFile = $('input[name=file]');
			var fileToUpload = inputFile[0].files[0];
			var ext = fileToUpload['name'].search('jpg');
			var ext1 = fileToUpload['name'].search('jpeg');
			if (ext > 0 || ext1 > 0) {
				if (fileToUpload) {
					$('#mb-crop').show();
					reader = new FileReader();
					reader.onload = function () {
						image.src = reader.result;
					}
					reader.readAsDataURL(fileToUpload);
					var learning_id = $("#learning_id").val();
					setTimeout(() => {
						$('#crop').show();
						$('#cancel').show();
						var cropper = new Cropper(image, {
							viewMode: 1,
							ready: function () {
								var image = new Image();
								$('#crop').click(function () {
									image.src = cropper.getCroppedCanvas()
										.toDataURL('image/jpeg');
									$result.src = image.src;
									$(".img-container").hide();
									$('#result').show();
									$('#restore').show();
									$('#save').show();
									$('#crop').hide();
								});

								$('#save').click(function () {
									var formData = new FormData();
									formData.append("image", image.src);
									$("#save").html('Loading..');
									$.ajax({
										url: 'simulation_controller/uploadQTest',
										type: 'post',
										data: formData,
										processData: false,
										contentType: false,
										success: function () {
											$("#save").html('Save');
											mSuccess(
												'File has been cropped'
											);
											loadView(
												'simulation_controller/QTest/' +
												learning_id,
												'.content');
										}
									});
								});

								$('#restore').click(function () {
									$(".img-container").show();
									$('#result').hide();
									$('#restore').hide();
									$('#save').hide();
									$('#crop').show();
								});

								$('#cancel').click(function () {
									loadView('simulation_controller/QTest' +
										learning_id,
										'.content');
								});
							},
						});
						$("#image").show();
					}, 500);
				}
			} else {
				mError("Only jpg file allowed");
				$("#fileInput").val('');
			}
		} else {
			mError("Maximum image has been reached");
			$("#fileInput").val('');
		}
	});

	function destroy(id) {
		mConfirm('Are you sure want to delete ?', "doDestroy('" + id + "')");
	}

	function doDestroy(id) {
		var learning_id = $("#learning_id").val();
		var data = {
			id: id
		}
		postData('main_controller/destroy/test_temps/id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView('simulation_controller/QTest/' + learning_id, '.content');
					mSuccess('Image has been deleted');
				} else {
					mError('Failed to delete image');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	
	function startTest() {
		$("#startTest").html('Loading ..');
		var learning_id = $("#learning_id").val()
		var data = {
			learning_id: learning_id,
			identity: $("#identity").val()
		}
		postData('simulation_controller/startQTest/', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#startTest").html('Start Test');
					mSuccess('Test complete');
					loadView("simulation_controller/QTest/" + learning_id, '.content');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

</script>

<style>
	.img-container img {
		max-width: 500px;
	}

	.mc-training {
		height: 500px;
		overflow-y: scroll;
	}

	.mc-training::-webkit-scrollbar {
		width: 12px;
	}

	.mc-training::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
		border-radius: 10px;
	}

	.mc-training::-webkit-scrollbar-thumb {
		border-radius: 10px;
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0);
	}

	.result-container {
		margin: 0 auto;
		width: 500px;
	}

	.mb-training .mc-training {
		top: 0;
		height: 100vh;
	}
</style>