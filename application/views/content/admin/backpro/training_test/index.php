<ul class="breadcrumb">
	<li>
		<a href="#">Backpropagation</a>
	</li>
	<li class="active">Training Data Test</li>
</ul>

<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
			<div class="form-horizontal">

				<div class="panel panel-default">
					<div class="panel-heading ui-draggable-handle">
						<h3 class="panel-title">
							<strong>Learning</strong> Sample</h3>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="col-md-3 col-xs-12 control-label">Choose Learning Sample</label>
									<div class="col-md-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-gear"></span></span>
											<select onchange="changeId($(this).val())" id="learning_id"
												class="form-control">
												<option value="">-Pilih Sample-</option>
												<?php foreach ($learning as $key => $value) { ?>
												<?php if($value['learning_id'] == $learning_id){

                                                    if($value['last_mse'] < $value['target_mse']){?>
												    <option selected value="<?php echo $value['learning_id'] ?>">Learning
													    <?php echo "{$value['learning_id']} | Data Training : {$value['train_data']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
                                                    </option>
                                                    <?php }else{?>
                                                        <option selected value="<?php echo $value['learning_id'] ?>" style="color: red">Learning
                                                            <?php echo "{$value['learning_id']} | Data Training : {$value['train_data']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
                                                        </option>
                                                    <?php } ?>
                                                <?php }else {
                                                    if($value['last_mse'] < $value['target_mse']){ ?>
                                                        <option value="<?php echo $value['learning_id'] ?>">Learning
                                                            <?php echo "{$value['learning_id']} | Data Training : {$value['train_data']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
                                                        </option>
                                                    <?php }else{?>
                                                        <option value="<?php echo $value['learning_id'] ?>" style="color: red">Learning
                                                            <?php echo "{$value['learning_id']} | Data Training : {$value['train_data']} | Epoch : {$value['last_epoch']} | MSE : {$value['last_mse']}" ?>
                                                        </option>
                                                    <?php } ?>
												<?php } } ?>
											</select>
											<span onclick="destroy()" class="input-group-addon"><i style="color: red; cursor: pointer;" class="fa fa-times"></i></span>
										</div>
										<span class="help-block" style="color: red">Sesuaikan Learning Sample yang dipilih dengan data training saat ini untuk hasil yang lebih akurat</span>
										<span class="help-block" style="color: red">Data Training saat ini : <?php echo $current_data ?></span>
									</div>
								</div>
							</div>
						</div>
						<br>
						<?php if($learning_id != 'null'){ ?>
						<div class="row">
							<div class="col-md-12">
								<div id="mseGraph"></div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12" style="text-align: center">
								<a onclick="startTest()" id="testBtn" class="btn btn-primary">Start Test</a>
							</div>
							<div class="col-md-12">
								<div id="testGraph"></div>
							</div>
						</div>
						<?php } ?>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#backTest_spin").hide();
	$("#backTest_normal").show();
	var learning_id = $("#learning_id").val();
	if (learning_id != '') {
		$(function () {
			var chart;
			$(document).ready(function () {
				$.getJSON("simulation_controller/mseGraph/" + learning_id, function (json) {

					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'mseGraph',
							type: 'line'

						},
						title: {
							text: 'MSE pada Sample ' + learning_id +
								' adalah <?php echo $learn_result ?>'

						},
						subtitle: {
							text: ''

						},
						credits: {
							enabled: false
						},
						xAxis: {
							categories: []
						},
						yAxis: {
							title: {
								text: 'Mean Square Error (MSE)'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.series.name + '</b><br/>' +
									this.x + ': ' + this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});

			});

		});

		$(function () {
			var chart;
			$(document).ready(function () {
				$.getJSON("simulation_controller/testGraph/" + learning_id, function (json) {

					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'testGraph',
							type: 'line'

						},
						title: {
							text: 'Perbandingan nilai sampel training dengan target (Sandart Quality)'

						},
						subtitle: {
							text: ''

						},
						credits: {
							enabled: false
						},
						xAxis: {
							categories: []
						},
						yAxis: {
							title: {
								text: 'Mean Square Error (MSE)'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.series.name + '</b><br/>' +
									this.x + ': ' + this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});

			});

		});

	}

	function startTest() {
		$("#testBtn").html('Loading ..');
		var data = {
			learning_id: $("#learning_id").val()
		}
		postData('simulation_controller/startTest/', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#testBtn").html('Start Test');
					mSuccess('Test complete');
					loadView("simulation_controller/backTest/" + learning_id, '.content');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	function changeId(learning_id) {
		loadView("simulation_controller/backTest/" + learning_id, '.content');
	}

	function destroy(){
		var id = $("#learning_id").val();
		if(id != ''){
			mConfirm('Are you sure want to delete Learning Sample '+id+' ?', "do_destroy('"+id+"')");
		}else{
			mError('Choose learning sample first');
		}
	}

	function do_destroy(id) {
		var data = {
			id: id
		}
		postData('main_controller/destroy/learnings/learning_id', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					loadView("simulation_controller/backTest/null", '.content');
					mSuccess('Learning Sample has been deleted');
				} else {
					mError('Failed to delete learning sample');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>