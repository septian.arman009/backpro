<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'main_controller';
$route['404_override'] = 'error_handling';
$route['translate_uri_dashes'] = false;

$route['signin'] = 'auth_controller';
$route['forgot_password'] = 'auth_controller/forgotPassword';
$route['password_reset/(:any)'] = 'auth_controller/passwordReset/$1';

$route['admin'] = 'main_controller';