<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('send_mail')) {
    function send_mail($email, $message, $subject)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $setting = $ci->main_model->getArray('settings');
        //Load email library
        $ci->load->library('email');
        //SMTP & mail configuration
        $config = array(
            'protocol' => $setting[0]['protocol'],
            'smtp_host' => $setting[0]['mail_host'],
            'smtp_port' => $setting[0]['mail_port'],
            'smtp_user' => $setting[0]['send_mail'],
            'smtp_pass' => $setting[0]['send_pass'],
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );
        $ci->email->initialize($config);
        $ci->email->set_mailtype("html");
        $ci->email->set_newline("\r\n");
        $ci->email->to($email);
        $ci->email->from($setting[0]['send_mail'], 'Apps - Backpropagation');
        $ci->email->subject($subject);
        $ci->email->message($message);
        //Send email
        $status = $ci->email->send();
        return $status;
    }
}

if (!function_exists('role')) {
    function role($role, $die)
    {
        $ci = &get_instance();
        if (in_array($_SESSION['back_in']['role'], $role)) {
            return true;
        } else {
            if ($die == true) {
                $status = array('status' => 'errors');
                $ci->mylib->setJSON();
                echo json_encode($status);
                die();
            }
        }
    }
}

if (!function_exists('auth')) {
    function auth()
    {
        $ci = &get_instance();
        if (!$ci->session->userdata('back_in')) {
            redirect('signin');
        }
    }
}

if (!function_exists('guest')) {
    function guest()
    {
        $ci = &get_instance();
        if ($ci->session->userdata('back_in')) {
            redirect('admin');
        }
    }
}

if (!function_exists('r_success')) {
    function r_success()
    {
        $ci = &get_instance();
        $status = array('status' => 'success', 'code' => 200);
        header('Content-Type:application/json');
        echo json_encode($status);
    }
}

if (!function_exists('response')) {
    function response($response, $code)
    {
        $ci = &get_instance();
        $status = array('status' => $response, 'code' => $code);
        header('Content-Type:application/json');
        echo json_encode($status);
    }
}

if (!function_exists('r_error')) {
    function r_error()
    {
        $ci = &get_instance();
        $status = array('status' => 'error');
        header('Content-Type:application/json');
        echo json_encode($status);
    }
}

if (!function_exists('r_exist')) {
    function r_exist()
    {
        $ci = &get_instance();
        $status = array('status' => 'exist');
        header('Content-Type:application/json');
        echo json_encode($status);
    }
}

if (!function_exists('r_success_data')) {
    function r_success_data($data)
    {
        $ci = &get_instance();
        $status = array('status' => 'success', 'code' => 200, 'data' => $data);
        header('Content-Type:application/json');
        echo json_encode($status);
    }
}

if (!function_exists('logs')) {
    function logs($message)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $log['user_id'] = $_SESSION['back_in']['id'];
        $log['name'] = $_SESSION['back_in']['name'];
        $log['log'] = $message;
        $ci->main_model->insert('logs', $log);
    }
}

if (!function_exists('do_action')) {
    function do_action($key, $table, $column, $data, $message)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        if ($key == 'null') {
            $insert = $ci->main_model->insert($table, $data);
            if ($insert) {
                logs($message);
                r_success();
            }
        } else {
            $update = $ci->main_model->update($table, $data, $column, $key);
            if ($update) {
                logs($message);
                r_success();
            }
        }
    }
}

if (!function_exists('show_table')) {
    function show_table($id, $table, $column, $param)
    {
        $ci = &get_instance();
        $ci->load->model('d_table');
        if (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            $datatables = $_POST;
            $datatables['table'] = $table;
            $datatables['id-table'] = $id;
            $datatables['param'] = $param;
            $datatables['col-display'] = $column;
            $ci->d_table->Datatables($datatables);
        }
        return;
    }
}

if (!function_exists('takeArrayValue')) {
    function takeArrayValue($data, $index)
    {
        $value_array = array();
        foreach ($data as $key => $value) {
            $value_array[] = $value[$index];
        }
        return $value_array;
    }
}

if (!function_exists('takeArrayValue1')) {
    function takeArrayValue1($data, $index, $target)
    {
        $arrayValue = array();
        foreach ($data as $key => $v1) {
            $qty = 0;
            foreach ($data as $key => $v2) {
                if ($v1[$index] == $v2[$index]) {
                    $qty += $v2[$target];
                }
            }
            $arrayValue[$v1[$index]] = $qty;
        }
        return $arrayValue;
    }
}

if (!function_exists('takeArrayValueWhere')) {
    function takeArrayValueWhere($data, $column, $column1, $where, $value)
    {
        $arrayValue = array();
        foreach ($data as $key => $v1) {
            if ($v1[$where] == $value) {
                $arrayValue[] = array(
                    $column => $v1[$column],
                    $column1 => $v1[$column1],
                );
            }
        }
        return $arrayValue;
    }
}

if (!function_exists('takeArrayValueToString')) {
    function takeArrayValueToString($data, $index)
    {
        $string = '';
        foreach ($data as $key => $value) {
            if ($string == '') {
                $string = '"' . $value[$index] . '"';
            } else {
                $string = $string . ',"' . $value[$index] . '"';
            }
        }
        return $string;
    }
}

if (!function_exists('to_json')) {
    function to_json()
    {
        $json = file_get_contents("php://input");
        return json_decode($json);
    }
}

if (!function_exists('whoIAM')) {
    function whoIAM()
    {
        return $data = array(
            'id' => $_SESSION['back_in']['id'],
            'name' => $_SESSION['back_in']['name'],
            'email' => $_SESSION['back_in']['email'],
            'role' => $_SESSION['back_in']['role'],
            'role_name' => $_SESSION['back_in']['role_name'],
        );
    }
}

if (!function_exists('to_rp')) {
    function to_rp($number)
    {
        if ($number >= 0) {
            return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($number)), 3)));
        } else {
            $num = explode('-', $number);
            return 'Rp. - ' . strrev(implode('.', str_split(strrev(strval($num[1])), 3)));
        }
    }
}

if (!function_exists('arrayKeyLast')) {
    function arrayKeyLast($array)
    {
        $key = null;
        if (is_array($array)) {
            end($array);
            $key = key($array);
        }
        return $key;
    }
}

if (!function_exists('arraySearch')) {
    function arraySearch($array, $field, $search)
    {
        foreach ($array as $key => $value) {
            if ($value[$field] === $search) {
                return $key;
            }
        }
        return false;
    }
}

if (!function_exists('arraySearch2')) {
    function arraySearch2($array, $field1, $search1, $field2, $search2)
    {
        foreach ($array as $key => $value) {
            if ($value[$field1] === $search1 && $value[$field2] === $search2) {
                return $key;
            }
        }
        return false;
    }
}

if (!function_exists('toDump')) {
    function toDump($data)
    {
        echo '<pre>';
        var_dump($data);
        die();
        echo '</pre>';
    }
}

if (!function_exists('randomString')) {
    function randomString($length)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $string;
    }
}

if (!function_exists('lastAi')) {
    function lastAi($front_code, $table)
    {
        $ci = &get_instance();
        $ci->load->model('main_model');
        $last_number = $ci->main_model->lastAi($table);

        if ($last_number < 10) {
            $last_number = '0' . $last_number;
        } else {
            $last_number = $last_number;
        }

        $code = $front_code . '' . $last_number;
        return $code;
    }
}

if (!function_exists('toDecimal')) {

    function toDecimal($folder, $image)
    {
        $colorArray = getArrayOfPixelsFromFile(base_url() . 'assets/' . $folder . '/' . $image);
        $index = 0;
        foreach ($colorArray as $key => $cValue) {
            foreach ($cValue as $key => $value) {
                // $color[$index][] = hexdec(sprintf("%02x%02x%02x", $value[0], $value[1], $value[2]));
                $color[$index][] = ($value[0] + $value[1] + $value[2]) / 3;
            }
            $index++;
        }

        foreach ($color as $key => $vc) {
            // $colorDecimal[] = array_sum(array_unique($vc));
            $colorDecimal[] = array_sum($vc);
        }

        // $decimalValue = array_sum(array_unique($colorDecimal));
        $decimalValue = array_sum($colorDecimal);

        return $decimalValue / count($colorArray);
    }
}

if (!function_exists('getArrayOfPixelsFromFile')) {

function getArrayOfPixelsFromFile($source)
{
    $image = imagecreatefromjpeg($source);

    $width = imagesx($image);
    $height = imagesy($image);
    $colors = array();

    for ($y = 0; $y < $height; $y++) {
        $y_array = array();
        for ($x = 0; $x < $width; $x++) {
            $rgb = imagecolorat($image, $x, $y);
            $r = ($rgb >> 16) & 0xFF;
            $g = ($rgb >> 8) & 0xFF;
            $b = $rgb & 0xFF;
            $x_array = array($r, $g, $b);
            $y_array[] = $x_array;
        }
        $colors[] = $y_array;
    }
    return $colors;
}

}