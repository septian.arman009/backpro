<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends Main_model
{

    public function Datatables($dt)
    {
        $columns = implode(', ', $dt['col-display']);
        $sql = "SELECT {$columns} FROM {$dt['table']}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);

        $where = '';
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if ($dt['table'] == 'users') {
                    if ($i == 3) {
                        $roles = $this->db->query('select role_id from roles where name like "%' . $searchCol . '%"')->result_array();
                        $in = takeArrayValueToString($roles, 'role_id');
                        if ($in != '') {
                            $where = $columnd[$i] . ' IN (' . $in . ')';
                        } else {
                            $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                        }
                    } else {
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                    }
                } else {
                    $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                }
                break;
            }
        }

        if ($where != '') {
            $sql .= " WHERE " . $where;
        }

        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);

        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();
            $id = 0;
            $data = 1;
            foreach ($dt['col-display'] as $key => $kolom) {

                if ($id == 0) {
                    $id = $val[$kolom];
                    $rows[] = $no;
                } else if ($dt['table'] == 'users' && $data == 4) {
                    $rows[] = $this->getOne('roles', 'display_name', 'role_id', $val[$kolom]);
                } else {
                    $rows[] = $val[$kolom];
                }
                
                $data++;
            }
            
            if($dt['table'] == 'trainings'){
                $rows[] =
                '<a title="Detail" class="btn btn-warning btn-xs waves-effect"
                onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-eye"></i></a>
                | <a title="Hapus" class="btn btn-danger btn-xs waves-effect"
                onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
            }else{
                $rows[] =
                '<a title="Edit" class="btn btn-info btn-xs waves-effect"
                onclick="edit(' . "'" . $id . "'" . ');""><i class="fa fa-pencil-square-o"></i></a>
                | <a title="Hapus" class="btn btn-danger btn-xs waves-effect"
                onclick="destroy(' . "'" . $id . "'" . ');"><i class="fa fa-trash-o"></i></a>';
            }
        
            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

}
