<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backpro_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function backTrain()
    {
        if (role(['admin'], false)) {
            $this->load->view('content/admin/backpro/learning');
        }
    }

    public function trainingStart()
    {
        $obj = to_json();
        $v11 = $obj->v11;
        $v21 = $obj->v21;
        $v31 = $obj->v31;
        $v41 = $obj->v41;
        $v51 = $obj->v51;
        $v12 = $obj->v12;
        $v22 = $obj->v22;
        $v32 = $obj->v32;
        $v42 = $obj->v42;
        $v52 = $obj->v52;
        $v13 = $obj->v13;
        $v23 = $obj->v23;
        $v33 = $obj->v33;
        $v43 = $obj->v43;
        $v53 = $obj->v53;
        $v14 = $obj->v14;
        $v24 = $obj->v24;
        $v34 = $obj->v34;
        $v44 = $obj->v44;
        $v54 = $obj->v54;
        $v15 = $obj->v15;
        $v25 = $obj->v25;
        $v35 = $obj->v35;
        $v45 = $obj->v45;
        $v55 = $obj->v55;

        $v01 = $obj->v01;
        $v02 = $obj->v02;
        $v03 = $obj->v03;
        $v04 = $obj->v04;
        $v05 = $obj->v05;

        $w0 = $obj->w0;

        $w1 = $obj->w1;
        $w2 = $obj->w2;
        $w3 = $obj->w3;
        $w4 = $obj->w4;
        $w5 = $obj->w5;

        $lrInput = $obj->lrInput;
        $lrBiasInput = $obj->lrBiasInput;
        $lrHidden = $obj->lrHidden;
        $lrBiasHidden = $obj->lrBiasHidden;

        $targetMse = $obj->mse;
        $maxEpoch = $obj->maxEpoch;
        $oldMse = 0;
        $newMse = 0;

        $limit = 900;
        $index_mse = 0;
        for ($i = 1; $i <= $maxEpoch; $i++) { //Iterasi Epoch
            $input = $this->mm->getArray("trainings");
            $length = count($input);
            $target = $this->mm->getArray('t_transform');
            $totalError = 0;
            $neuralOutput = array();
            foreach ($input as $key => $value) { //Inisial Input

                $x1 = $value['d1n'];
                $x2 = $value['d2n'];
                $x3 = $value['d3n'];
                $x4 = $value['d4n'];
                $x5 = $value['d5n'];
                $t = $target[0]['target'];

                //Perkalian unit tersembunyi
                $z_net1 = $v01 + ($x1 * $v11) + ($x2 * $v21) + ($x3 * $v31) + ($x4 * $v41) + ($x5 * $v51);
                $z_net2 = $v02 + ($x1 * $v12) + ($x2 * $v22) + ($x3 * $v32) + ($x4 * $v42) + ($x5 * $v52);
                $z_net3 = $v03 + ($x1 * $v13) + ($x2 * $v23) + ($x3 * $v33) + ($x4 * $v43) + ($x5 * $v53);
                $z_net4 = $v04 + ($x1 * $v14) + ($x2 * $v24) + ($x3 * $v34) + ($x4 * $v44) + ($x5 * $v54);
                $z_net5 = $v05 + ($x1 * $v15) + ($x2 * $v25) + ($x3 * $v35) + ($x4 * $v45) + ($x5 * $v55);

                //Pengaktifan unit tersembunyi (sigmoid biner)
                $z1 = 1 / (1 + (pow(2.71828183, -$z_net1)));
                $z2 = 1 / (1 + (pow(2.71828183, -$z_net2)));
                $z3 = 1 / (1 + (pow(2.71828183, -$z_net3)));
                $z4 = 1 / (1 + (pow(2.71828183, -$z_net4)));
                $z5 = 1 / (1 + (pow(2.71828183, -$z_net5)));

                //Perkalian unit keluaran
                $y_net = $w0 + ($z1 * $w1) + ($z2 * $w2) + ($z3 * $w3) + ($z4 * $w4) + ($z5 * $w5);

                //Pengaktifan unit keluaran (identitas)
                $y = $y_net;
                $error = $t - $y;
                $totalError = $totalError + ($error * $error);

                //Faktor kesalahan pertama
                $dk0 = $error;

                //Perubahan Bobot unit keluaran
                $w0_n = $lrBiasHidden * $dk0;
                $w1_n = $lrHidden * $dk0 * $z1;
                $w2_n = $lrHidden * $dk0 * $z2;
                $w3_n = $lrHidden * $dk0 * $z3;
                $w4_n = $lrHidden * $dk0 * $z4;
                $w5_n = $lrHidden * $dk0 * $z5;

                //Suku perubahan Bobot unit tersembunyi #1
                $d_net1 = $dk0 * $w1;
                $dj1 = ($d_net1 * $z1) * (1 - $z1);
                //Perubahan bias z1(v01)
                $v01_n = $lrBiasInput * $dj1;
                //Suku perubahan bobot z1(v11,v21)
                $v11_n = $lrInput * $dj1 * $x1;
                $v21_n = $lrInput * $dj1 * $x2;
                $v31_n = $lrInput * $dj1 * $x3;
                $v41_n = $lrInput * $dj1 * $x4;
                $v51_n = $lrInput * $dj1 * $x5;

                $d_net2 = $dk0 * $w2;
                $dj2 = ($d_net2 * $z2) * (1 - $z2);
                $v02_n = $lrBiasInput * $dj2;
                $v12_n = $lrInput * $dj2 * $x1;
                $v22_n = $lrInput * $dj2 * $x2;
                $v32_n = $lrInput * $dj2 * $x3;
                $v42_n = $lrInput * $dj2 * $x4;
                $v52_n = $lrInput * $dj2 * $x5;

                $d_net3 = $dk0 * $w3;
                $dj3 = ($d_net3 * $z3) * (1 - $z3);
                $v03_n = $lrBiasInput * $dj3;
                $v13_n = $lrInput * $dj3 * $x1;
                $v23_n = $lrInput * $dj3 * $x2;
                $v33_n = $lrInput * $dj3 * $x3;
                $v43_n = $lrInput * $dj3 * $x4;
                $v53_n = $lrInput * $dj3 * $x5;

                $d_net4 = $dk0 * $w4;
                $dj4 = ($d_net4 * $z4) * (1 - $z4);
                $v04_n = $lrBiasInput * $dj4;
                $v14_n = $lrInput * $dj4 * $x1;
                $v24_n = $lrInput * $dj4 * $x2;
                $v34_n = $lrInput * $dj4 * $x3;
                $v44_n = $lrInput * $dj4 * $x4;
                $v54_n = $lrInput * $dj4 * $x5;

                $d_net5 = $dk0 * $w5;
                $dj5 = ($d_net5 * $z5) * (1 - $z5);
                $v05_n = $lrBiasInput * $dj5;
                $v15_n = $lrInput * $dj5 * $x1;
                $v25_n = $lrInput * $dj5 * $x2;
                $v35_n = $lrInput * $dj5 * $x3;
                $v45_n = $lrInput * $dj5 * $x4;
                $v55_n = $lrInput * $dj5 * $x5;

                //Bobot Akhir Iterasi Pertama Input ke Hidden
                $v11 = $v11 + $v11_n;
                $v12 = $v12 + $v12_n;
                $v13 = $v13 + $v13_n;
                $v14 = $v14 + $v14_n;
                $v15 = $v15 + $v15_n;
                $v21 = $v21 + $v21_n;
                $v22 = $v22 + $v22_n;
                $v23 = $v23 + $v23_n;
                $v24 = $v24 + $v24_n;
                $v25 = $v25 + $v25_n;
                $v31 = $v31 + $v31_n;
                $v32 = $v32 + $v32_n;
                $v33 = $v33 + $v33_n;
                $v34 = $v34 + $v34_n;
                $v35 = $v35 + $v35_n;
                $v41 = $v41 + $v41_n;
                $v42 = $v42 + $v42_n;
                $v43 = $v43 + $v43_n;
                $v44 = $v44 + $v44_n;
                $v45 = $v45 + $v45_n;
                $v51 = $v51 + $v51_n;
                $v52 = $v52 + $v52_n;
                $v53 = $v53 + $v53_n;
                $v54 = $v54 + $v54_n;
                $v55 = $v55 + $v55_n;

                //Bias Akhir Iterasi Pertama Input ke Hidden
                $v01 = $v01 + $v01_n;
                $v02 = $v02 + $v02_n;
                $v03 = $v03 + $v03_n;
                $v04 = $v04 + $v04_n;
                $v05 = $v05 + $v05_n;

                //Bobot Akhir Iterasi Pertama Hiden ke Output
                $w1 = $w1 + $w1_n;
                $w2 = $w2 + $w2_n;
                $w3 = $w3 + $w3_n;
                $w4 = $w4 + $w4_n;
                $w5 = $w5 + $w5_n;

                //Bias Akhir Iterasi Pertama Hiden ke Output
                $w0 = $w0 + $w0_n;
                //Array penyimpan hasil akhir keluaran jaringan
                $neuralOutput[] = $y;
            } //Iterasi Nilai Input
            $oldMse = $newMse;
            $newMse = number_format(($totalError / $length), 10);

            if ($i % $limit == 0) {
                $limit += 900;
                $index_mse++;
            }

            $arrayMse[$index_mse][] = $newMse;

            if ($i > 1) {
                if ($newMse < $targetMse) {
                    $status = 'success';
                    break;
                } else if ($newMse > $oldMse) {
                    $status = 'mse_stop';
                    break;
                } else if ($i == $maxEpoch) {
                    $status = 'max_epoch';
                    break;
                }
            }

        } //Iterasi Epochs

        $x1 = array('v11' => $v11, 'v12' => $v12, 'v13' => $v13, 'v14' => $v14, 'v15' => $v15);
        $x2 = array('v21' => $v21, 'v22' => $v22, 'v23' => $v23, 'v24' => $v24, 'v25' => $v25);
        $x3 = array('v31' => $v31, 'v32' => $v32, 'v33' => $v33, 'v34' => $v34, 'v35' => $v35);
        $x4 = array('v41' => $v41, 'v42' => $v42, 'v43' => $v43, 'v44' => $v44, 'v45' => $v45);
        $x5 = array('v51' => $v51, 'v52' => $v52, 'v53' => $v53, 'v54' => $v54, 'v55' => $v55);
        $b1 = array('v01' => $v01, 'v02' => $v02, 'v03' => $v03, 'v04' => $v04, 'v05' => $v05);
        $w = array('w0' => $w0, 'w1' => $w1, 'w2' => $w2, 'w3' => $w3, 'w4' => $w4, 'w5' => $w5);

        $learn_data = array(
            'train_data' => $length,
            'last_mse' => $newMse,
            'target_mse' => $targetMse,
            'last_epoch' => $i,
            'target' => $target[0]['target'],
            'status' => $status,
        );

        $learning_id = $this->mm->insertGetId('learnings', $learn_data);
        if ($learning_id) {
            $learn_weight = array(
                'learning_id' => $learning_id,
                'x1' => serialize($x1),
                'x2' => serialize($x2),
                'x3' => serialize($x3),
                'x4' => serialize($x4),
                'x5' => serialize($x5),
                'b1' => serialize($b1),
                'w' => serialize($w),
            );
            $insert_weight = $this->mm->insert('learn_weight', $learn_weight);
            if ($insert_weight) {
                $learn_param = array(
                    'learning_id' => $learning_id,
                    'lr_input' => $lrInput,
                    'lr_bias_input' => $lrBiasInput,
                    'lr_hidden' => $lrHidden,
                    'lr_bias_hidden' => $lrBiasHidden,
                    'target_mse' => $targetMse,
                    'max_epoch' => $maxEpoch,
                );
                $insert_param = $this->mm->insert('learn_param', $learn_param);
                if ($learn_param) {
                    $learn_output = array(
                        'learning_id' => $learning_id,
                        'outputs' => serialize($neuralOutput),
                    );
                    $insert_output = $this->mm->insert('learn_output', $learn_output);
                    if ($insert_output) {
                        foreach ($arrayMse as $key => $value) {
                            $data_mse[] = array(
                                'learning_id' => $learning_id,
                                'mse' => serialize($value),
                            );
                        }
                        $insert_mse = $this->mm->insertMultiple('learn_mse', $data_mse);
                        if ($insert_mse) {
                            logs("New Learning Data has been added with status : {$status}");
                            response($status, 200);
                        }
                    }
                }
            }
        }
    }

}
