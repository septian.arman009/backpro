<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        guest();
    }

    public function index()
    {
        $this->load->view('auth/signin');
    }

    public function forgotPassword()
    {
        $this->load->view('auth/forgot_password');
    }

    public function signinProccess()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $password = md5($obj->password);

        $user = $this->mm->getArray2Where('users', 'email', $email, 'password', $password);

        if ($user) {
            $session_array = array();
            foreach ($user as $key => $val) {
                $roles = $this->mm->getArray('roles', 'role_id', $val['role_id']);
                $session_array = array(
                    'id' => $val['user_id'],
                    'name' => $val['name'],
                    'email' => $val['email'],
                    'password' => $val['password'],
                    'role' => $roles[0]['name'],
                    'role_name' => $roles[0]['display_name'],
                );
            }
            $this->session->set_userdata('back_in', $session_array);
            r_success();
        }
    }

    public function sendToken()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;

        $data['token'] = randomString(40);
        $data['email'] = $email;
        $url = base_url() . 'password_reset/' . $data['token'];
        $message = 'This is the link to reset your password : ' . $url;

        $send_mail = send_mail($email, $message, 'Password Reset Backpropagation Admin');
        if ($send_mail) {
            $this->mm->insert('tokens', $data);
            r_success();
        }

    }

    public function passwordReset($token)
    {
        $check = $this->mm->getArrayWhere('tokens', 'token', $token);
        if (!$check) {
            $this->load->view('auth/token_error');
        } else {
            $data['email'] = $check[0]['email'];
            $this->load->view('auth/password_reset', $data);
        }
    }

    public function resetPassword()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $email = $obj->email;
        $data['password'] = md5($obj->password);

        $reset = $this->mm->update('users', $data, 'email', $email);
        if ($reset) {
            $this->mm->destroy('tokens', 'email', $email);
            r_success();
        }
    }
}
