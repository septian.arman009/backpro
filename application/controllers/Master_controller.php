<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function form($form, $key_id, $param)
    {
        $data['key_id'] = $key_id;
        if ($form == 'users') {
            $data['roles'] = $this->mm->getArray('roles');
            $this->load->view('content/admin/master/user/form', $data);
        }
    }

    public function user()
    {
        if (role(['admin'], false)) {
            $this->load->view('content/admin/master/user/index');
        }
    }

    public function userTable()
    {
        $id = 'user_id';
        $table = 'users';
        $column = array(
            'user_id',
            'name',
            'email',
            'role_id',
        );
        show_table($id, $table, $column, 'null');
    }

    public function userAction()
    {
        $obj = to_json();
        $key_id = $obj->key_id;
        $table = $obj->table;
        $data['name'] = ucwords($obj->name);
        $data['email'] = $obj->email;
        $data['role_id'] = $obj->role_id;
        if ($key_id == 'null') {
            $data['password'] = md5($obj->password);
            $log_message = "Registered new user with email : {$data['email']}";
            do_action($key_id, $table, 'user_id', $data, $log_message);
        } else {
            $log_message = "Update user with email : {$data['email']}";
            do_action($key_id, $table, 'user_id', $data, $log_message);
        }
    }

    public function tTUpload()
    {
        if (role(['admin'], false)) {
            $data['tTemps'] = $this->mm->getArray('training_temps');
            $data['tImage'] = count($data['tTemps']);
            $this->load->view('content/admin/master/training_temp/index', $data);
        }
    }

    public function targetUpload()
    {
        if (role(['admin'], false)) {
            $data['target'] = $this->mm->getArray('target');
            $data['tImage'] = count($data['target']);
            $this->load->view('content/admin/master/target/index', $data);
        }
    }

    public function saveTUpload()
    {
        $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->input->post("image")));
        $image_name = md5(uniqid(rand(), true));
        $filename = $image_name . '.' . 'jpg';
        $path = './assets/t_temps/';
        file_put_contents($path . $filename, $image);

        $data['name'] = $filename;
        $data['rgb_value'] = toDecimal('t_temps', $filename);
        $insert = $this->mm->insert('training_temps', $data);
    }

    public function saveTarget()
    {
        $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->input->post("image")));
        $image_name = md5(uniqid(rand(), true));
        $filename = $image_name . '.' . 'jpg';
        $path = './assets/target/';
        file_put_contents($path . $filename, $image);

        $data['name'] = $filename;
        $data['rgb_value'] = toDecimal('target', $filename);
        $insert = $this->mm->insert('target', $data);
        if ($insert) {
            logs("Upload new target");
        }
    }

    public function updateTgtTrans()
    {
        $max = $this->mm->maxInColumn('target', 'rgb_value');
        $min = $this->mm->minInColumn('target', 'rgb_value');
        $target = $this->mm->getArray('target');
        $t1 = ((0.8 * ($target[0]['rgb_value'] - $min)) / ($max - $min)) + 0.1;
        $t2 = ((0.8 * ($target[1]['rgb_value'] - $min)) / ($max - $min)) + 0.1;
        $t3 = ((0.8 * ($target[2]['rgb_value'] - $min)) / ($max - $min)) + 0.1;
        $t4 = ((0.8 * ($target[3]['rgb_value'] - $min)) / ($max - $min)) + 0.1;
        $t5 = ((0.8 * ($target[4]['rgb_value'] - $min)) / ($max - $min)) + 0.1;

        $data['target'] = (($t1 + $t2 + $t3 + $t4 + $t5) / 5);
        $check = $this->mm->getArray('t_transform');
        if (!$check) {
            $insert = $this->mm->insert('t_transform', $data);
            $insert = $this->mm->update('t_transform', $data, 'id', 1);
            if ($insert) {
                r_success();
            }
        } else {
            $update = $this->mm->update('t_transform', $data, 'id', 1);
            if ($update) {
                r_success();
            }
        }
    }

    public function moveToTrain()
    {
        $tTemp = $this->mm->getArray("training_temps");
        $data['name'] = lastAi('Sample ', 'trainings');
        $data['d1o'] = $tTemp[0]['rgb_value'];
        $data['d2o'] = $tTemp[1]['rgb_value'];
        $data['d3o'] = $tTemp[2]['rgb_value'];
        $data['d4o'] = $tTemp[3]['rgb_value'];
        $data['d5o'] = $tTemp[4]['rgb_value'];
        foreach ($tTemp as $key => $value) {
            $img[] = $value['name'];
        }

        $data['image'] = serialize($img);
        $insert = $this->mm->insert('trainings', $data);
        if ($insert) {
            $this->db->truncate('training_temps');
            logs("Save new training data");
            r_success();
        }
    }

    public function training()
    {
        if (role(['admin'], false)) {
            $this->load->view('content/admin/master/training/index');
        }
    }

    public function trainingTable()
    {
        $id = 'training_id';
        $table = 'trainings';
        $column = array(
            'training_id',
            'name',
            'd1o',
            'd2o',
            'd3o',
            'd4o',
            'd5o',
        );
        show_table($id, $table, $column, 'null');
    }

    public function transformationTable()
    {
        $id = 'training_id';
        $table = 'trainings';
        $column = array(
            'training_id',
            'name',
            'd1n',
            'd2n',
            'd3n',
            'd4n',
            'd5n',
        );
        show_table($id, $table, $column, 'null');
    }

    public function trainingDetail($id)
    {
        $data['training'] = $this->mm->getArrayWhere('trainings', 'training_id', $id);
        $data['image'] = unserialize($data['training'][0]['image']);
        $this->load->view('content/admin/master/training/trainingDetail', $data);
    }

    public function trainTransform()
    {
        $trainData = $this->mm->getArray('trainings');
        $d1Max = $this->mm->maxInColumn('trainings', 'd1o');
        $d2Max = $this->mm->maxInColumn('trainings', 'd2o');
        $d3Max = $this->mm->maxInColumn('trainings', 'd3o');
        $d4Max = $this->mm->maxInColumn('trainings', 'd4o');
        $d5Max = $this->mm->maxInColumn('trainings', 'd5o');

        $d1Min = $this->mm->minInColumn('trainings', 'd1o');
        $d2Min = $this->mm->minInColumn('trainings', 'd2o');
        $d3Min = $this->mm->minInColumn('trainings', 'd3o');
        $d4Min = $this->mm->minInColumn('trainings', 'd4o');
        $d5Min = $this->mm->minInColumn('trainings', 'd5o');

        foreach ($trainData as $key => $value) {
            $transform[] = array(
                'training_id' => $value['training_id'],
                'd1n' => ((0.8 * ($value['d1o'] - $d1Min)) / ($d1Max - $d1Min)) + 0.1,
                'd2n' => ((0.8 * ($value['d2o'] - $d2Min)) / ($d2Max - $d2Min)) + 0.1,
                'd3n' => ((0.8 * ($value['d3o'] - $d3Min)) / ($d3Max - $d3Min)) + 0.1,
                'd4n' => ((0.8 * ($value['d4o'] - $d4Min)) / ($d4Max - $d4Min)) + 0.1,
                'd5n' => ((0.8 * ($value['d5o'] - $d5Min)) / ($d5Max - $d5Min)) + 0.1,
            );
        }

        $update = $this->mm->updateMultiple('trainings', $transform, 'training_id');
        if ($update) {
            logs("New normalize of training data");
            r_success();
        }
    }
}
