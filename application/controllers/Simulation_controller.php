<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Simulation_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model', 'mm');
        auth();
    }

    public function backTest($learning_id)
    {
        if (role(['admin'], false)) {
            $data['learning_id'] = $learning_id;
            if ($learning_id != 'null') {
                $learn = $this->mm->getArrayWhere('learnings', 'learning_id', $learning_id);
                $data['learn_result'] = "{$learn[0]['last_mse']} pada Epoch Ke-{$learn[0]['last_epoch']}";
            } else {
                $data['learn_result'] = '---';
            }
            $data['learning'] = $this->mm->getArray('learnings');
            $data['current_data'] = $this->mm->countRow('trainings');
            $this->load->view('content/admin/backpro/training_test/index', $data);
        }
    }

    public function mseGraph($learning_id)
    {
        $mse = $this->mm->getArrayWhere('learn_mse', 'learning_id', $learning_id);
        $mse_value = unserialize($mse[0]['mse']);
        $row['data'][] = null;
        foreach ($mse_value as $key => $value) {
            $row['data'][] = $value;
        }

        $row['name'] = 'Epoch';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function testGraph($learning_id)
    {
        $t_test = unserialize($this->mm->getOne('training_test', 't_test', 'learning_id', $learning_id));
        $target = $this->mm->getOne('learnings', 'target', 'learning_id', $learning_id);

        $row['data'][] = null;
        $row1['data'][] = null;
        if ($t_test) {
            foreach ($t_test as $key => $value) {
                $row['data'][] = $value;
                $row1['data'][] = $target;
            }
        }

        $row['name'] = 'Keluaran JST';
        $row1['name'] = 'Target';
        $result = array();
        array_push($result, $row);
        array_push($result, $row1);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function QTest($learning_id)
    {
        $data['learning_id'] = $learning_id;
        if (role(['admin'], false)) {
            $data['learning'] = $this->mm->getArray('learnings');
            $tImage = $this->mm->getArray('test_temps');
            $data['testData'] = $tImage;
            $data['tImage'] = count($tImage);
            $this->load->view('content/admin/backpro/qc_test/index', $data);
        }
    }

    public function QTestGraph($learning_id)
    {
        $data['learning_id'] = $learning_id;
        $data['mse'] = $this->mm->getOne('learnings', 'last_mse', 'learning_id', $learning_id);
        $this->load->view('content/admin/backpro/qc_test/QTestGraph', $data);
    }

    public function uploadQTest()
    {
        $image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->input->post("image")));
        $image_name = md5(uniqid(rand(), true));
        $filename = $image_name . '.' . 'jpg';
        $path = './assets/q_test/';
        file_put_contents($path . $filename, $image);

        $data['name'] = $filename;
        $data['rgb_value'] = toDecimal('q_test', $filename);
        $insert = $this->mm->insert('test_temps', $data);
    }

    public function startQTest()
    {
        $obj = to_json();
        $learning_id = $obj->learning_id;
        $identity = $obj->identity;
        $learn_weight = $this->mm->getArrayWhere('learn_weight', 'learning_id', $learning_id);

        $v1 = unserialize($learn_weight[0]['x1']);
        $v2 = unserialize($learn_weight[0]['x2']);
        $v3 = unserialize($learn_weight[0]['x3']);
        $v4 = unserialize($learn_weight[0]['x4']);
        $v5 = unserialize($learn_weight[0]['x5']);
        $v0 = unserialize($learn_weight[0]['b1']);
        $w = unserialize($learn_weight[0]['w']);

        $d1Max = $this->mm->maxInColumn('trainings', 'd1o');
        $d2Max = $this->mm->maxInColumn('trainings', 'd2o');
        $d3Max = $this->mm->maxInColumn('trainings', 'd3o');
        $d4Max = $this->mm->maxInColumn('trainings', 'd4o');
        $d5Max = $this->mm->maxInColumn('trainings', 'd5o');

        $d1Min = $this->mm->minInColumn('trainings', 'd1o');
        $d2Min = $this->mm->minInColumn('trainings', 'd2o');
        $d3Min = $this->mm->minInColumn('trainings', 'd3o');
        $d4Min = $this->mm->minInColumn('trainings', 'd4o');
        $d5Min = $this->mm->minInColumn('trainings', 'd5o');

        $input = $this->mm->getArray('test_temps');

        $x1 = ((0.8 * ($input[0]['rgb_value'] - $d1Min)) / ($d1Max - $d1Min)) + 0.1;
        $x2 = ((0.8 * ($input[1]['rgb_value'] - $d2Min)) / ($d2Max - $d2Min)) + 0.1;
        $x3 = ((0.8 * ($input[2]['rgb_value'] - $d3Min)) / ($d3Max - $d3Min)) + 0.1;
        $x4 = ((0.8 * ($input[3]['rgb_value'] - $d4Min)) / ($d4Max - $d4Min)) + 0.1;
        $x5 = ((0.8 * ($input[4]['rgb_value'] - $d5Min)) / ($d5Max - $d5Min)) + 0.1;

        //Perkalian unit tersembunyi
        $z_net1 = $v0['v01'] + ($x1 * $v1['v11']) + ($x2 * $v2['v21']) + ($x3 * $v3['v31']) + ($x4 * $v4['v41']) + ($x5 * $v5['v51']);
        $z_net2 = $v0['v02'] + ($x1 * $v1['v12']) + ($x2 * $v2['v22']) + ($x3 * $v3['v32']) + ($x4 * $v4['v42']) + ($x5 * $v5['v52']);
        $z_net3 = $v0['v03'] + ($x1 * $v1['v13']) + ($x2 * $v2['v23']) + ($x3 * $v3['v33']) + ($x4 * $v4['v43']) + ($x5 * $v5['v53']);
        $z_net4 = $v0['v04'] + ($x1 * $v1['v14']) + ($x2 * $v2['v24']) + ($x3 * $v3['v34']) + ($x4 * $v4['v44']) + ($x5 * $v5['v54']);
        $z_net5 = $v0['v05'] + ($x1 * $v1['v15']) + ($x2 * $v2['v25']) + ($x3 * $v3['v35']) + ($x4 * $v4['v45']) + ($x5 * $v5['v55']);

        //Pengaktifan unit tersembunyi (sigmoid biner)
        $z1 = 1 / (1 + (pow(2.71828183, -$z_net1)));
        $z2 = 1 / (1 + (pow(2.71828183, -$z_net2)));
        $z3 = 1 / (1 + (pow(2.71828183, -$z_net3)));
        $z4 = 1 / (1 + (pow(2.71828183, -$z_net4)));
        $z5 = 1 / (1 + (pow(2.71828183, -$z_net5)));

        //Perkalian unit keluaran
        $y_net = $w['w0'] + ($z1 * $w['w1']) + ($z2 * $w['w2']) + ($z3 * $w['w3']) + ($z4 * $w['w4']) + ($z5 * $w['w5']);

        $data['learning_id'] = $learning_id;
        $data['identity'] = $identity;

        $data['x1'] = $x1;
        $data['x2'] = $x2;
        $data['x3'] = $x3;
        $data['x4'] = $x4;
        $data['x5'] = $x5;

        $data['result'] = $y_net;
        $data['target'] = $this->mm->getOne('learnings', 'target', 'learning_id', $learning_id);
        toDump($y_net);

        $check = $this->mm->getArray2Where('test_result', 'learning_id', $learning_id, 'identity', $identity);
        if (!$check) {
            $insert = $this->mm->insert('test_result', $data);
            if ($insert) {
                r_success();
            }
        } else {
            $update = $this->mm->update2where('test_result', $data, 'learning_id', $learning_id, 'identity', $identity);
            if ($update) {
                r_success();
            }
        }
    }

    public function startTest()
    {
        $obj = to_json();
        $learning_id = $obj->learning_id;
        $learn_weight = $this->mm->getArrayWhere('learn_weight', 'learning_id', $learning_id);

        $v1 = unserialize($learn_weight[0]['x1']);
        $v2 = unserialize($learn_weight[0]['x2']);
        $v3 = unserialize($learn_weight[0]['x3']);
        $v4 = unserialize($learn_weight[0]['x4']);
        $v5 = unserialize($learn_weight[0]['x5']);
        $v0 = unserialize($learn_weight[0]['b1']);
        $w = unserialize($learn_weight[0]['w']);

        $input = $this->mm->getArray('trainings');
        foreach ($input as $key => $value) {
            $x1 = $value['d1n'];
            $x2 = $value['d2n'];
            $x3 = $value['d3n'];
            $x4 = $value['d4n'];
            $x5 = $value['d5n'];

            //Perkalian unit tersembunyi
            $z_net1 = $v0['v01'] + ($x1 * $v1['v11']) + ($x2 * $v2['v21']) + ($x3 * $v3['v31']) + ($x4 * $v4['v41']) + ($x5 * $v5['v51']);
            $z_net2 = $v0['v02'] + ($x1 * $v1['v12']) + ($x2 * $v2['v22']) + ($x3 * $v3['v32']) + ($x4 * $v4['v42']) + ($x5 * $v5['v52']);
            $z_net3 = $v0['v03'] + ($x1 * $v1['v13']) + ($x2 * $v2['v23']) + ($x3 * $v3['v33']) + ($x4 * $v4['v43']) + ($x5 * $v5['v53']);
            $z_net4 = $v0['v04'] + ($x1 * $v1['v14']) + ($x2 * $v2['v24']) + ($x3 * $v3['v34']) + ($x4 * $v4['v44']) + ($x5 * $v5['v54']);
            $z_net5 = $v0['v05'] + ($x1 * $v1['v15']) + ($x2 * $v2['v25']) + ($x3 * $v3['v35']) + ($x4 * $v4['v45']) + ($x5 * $v5['v55']);

            //Pengaktifan unit tersembunyi (sigmoid biner)
            $z1 = 1 / (1 + (pow(2.71828183, -$z_net1)));
            $z2 = 1 / (1 + (pow(2.71828183, -$z_net2)));
            $z3 = 1 / (1 + (pow(2.71828183, -$z_net3)));
            $z4 = 1 / (1 + (pow(2.71828183, -$z_net4)));
            $z5 = 1 / (1 + (pow(2.71828183, -$z_net5)));

            //Perkalian unit keluaran
            $y_net = $w['w0'] + ($z1 * $w['w1']) + ($z2 * $w['w2']) + ($z3 * $w['w3']) + ($z4 * $w['w4']) + ($z5 * $w['w5']);

            //Pengaktifan unit keluaran (identitas)
            $y[] = $y_net;
        }

        $data['learning_id'] = $learning_id;
        $data['t_test'] = serialize($y);
        $check = $this->mm->getArrayWhere('training_test', 'learning_id', $learning_id);
        if (!$check) {
            $insert = $this->mm->insert('training_test', $data);
            if ($insert) {
                r_success();
            }
        } else {
            $update = $this->mm->update('training_test', $data, 'learning_id', $learning_id);
            if ($update) {
                r_success();
            }
        }
    }

}
